package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/walle/targz"
	"golang.org/x/mod/semver"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"runtime"
	"strings"
)

var version = "v1.0.4"
var doxaHome string
var doxaBin string
var localVersionFile string
var previousDir string
var previousDoxaFile string
var previousDoxaVersionFile string
var currentDoxaFile string
var tempDownload string
var releaseInfoUrl = "https://github.com/liturgiko/doxa/raw/main/release.json"
var releaseInfo ReleaseInfo
var userHome string

const (
	NotInstalled               = 0
	InstalledAndCurrent        = 1
	InstalledAndNotCurrent     = 2
	InstalledAndAheadOfCurrent = 3
)

// main runs the doxa installer, getdoxa.
// getdoxa is downloaded and copied into the user's $HOME/doxa/bin directory.
// When getdoxa is run, it checks to see if there is a more recent version of doxa available.
// If so, it asks the user whether he/she wants getdoxa to install it.
// If doxa has already been installed, it is copied into $HOME/doxa/bin/.previous.
// This allows the user to restore doxa to its most immediate previous version, but no earlier.
// The first time doxa is installed, the user's PATH is updated to include $HOME/doxa/bin.
func main() {
	fmt.Printf("doxa installer version %s\n", version)
	var err error
	// set the global paths
	setPaths()

	// read the release info from the remote repo
	err = getReleaseInfo(releaseInfoUrl)
	if err != nil {
		log.Fatal(err)
	}
	// check for arguments passed by the user
	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "help", "-help", "--help":
			fmt.Println("getdoxa installs or updates doxa on your computer")
		case "restore", "-restore", "--restore":
			restore()
		default:
			check()
		}
	} else {
		check()
	}
}

// setPaths sets the values for the global path variables
func setPaths() {
	var err error
	userHome, err = os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	doxaHome = path.Join(userHome, "doxa")
	doxaBin = path.Join(doxaHome, "bin")
	localVersionFile = path.Join(doxaBin, ".ver")
	currentDoxaFile = path.Join(doxaBin, "doxa")
	previousDir = path.Join(doxaBin, ".previous")
	previousDoxaFile = path.Join(previousDir, ".doxa")
	previousDoxaVersionFile = path.Join(previousDir, ".ver")
	tempDownload = path.Join(doxaBin, "tmp")
}

// check determines whether a more recent version of doxa is available
// and informs the user of the result of the check.
func check() {
	fmt.Printf("\n\nChecking for latest version...\n\n")
	available, localVer := newVersionAvailable()
	switch available {
	case NotInstalled:
		fmt.Println("Doxa has not been installed yet.")
		install(localVer, releaseInfo.ReleaseId)
	case InstalledAndCurrent:
		fmt.Printf("You already have the latest version of Doxa (%s).", localVer)
	case InstalledAndNotCurrent:
		fmt.Printf("You have Doxa %s. The latest version is %s.\n", localVer, releaseInfo.ReleaseId)
		install(localVer, releaseInfo.ReleaseId)
	case InstalledAndAheadOfCurrent:
		fmt.Printf("You have Doxa %s, which is ahead of the latest published version %s.\n", localVer, releaseInfo.ReleaseId)
	}
}

// newVersionAvailable compares the local version to the remote and
// indicates whether there is a newer version available using a simulated enum using an int
// It also returns the local version number.
func newVersionAvailable() (int, string) {
	var localVer string
	localVer = getLocalVersionNumber()
	if len(localVer) == 0 {
		return NotInstalled, localVer
	}
	status := semver.Compare(releaseInfo.ReleaseId, localVer)
	switch status {
	case 0:
		return InstalledAndCurrent, localVer
	case 1:
		return InstalledAndNotCurrent, localVer
	default:
		return InstalledAndAheadOfCurrent, localVer
	}
}

// getLocalVersionNumber reads the doxa/bin/.ver file
// If it exists, it returns the version number.
// If it does not exist, or the version number is missing for the .ver file,
// or the bin/doxa file is missing,
// it returns an empty string.
func getLocalVersionNumber() string {
	if !FileExists(currentDoxaFile) {
		return ""
	}
	if !FileExists(localVersionFile) {
		return ""
	}
	lines, _ := GetFileLines(localVersionFile)
	if len(lines) > 0 {
		return lines[0]
	}
	return ""
}

// getReleaseBinaryInfo returns the information to download a binary for the user's OS and architecture
func getReleaseBinaryInfo() (ReleaseBinary, error) {
	var b ReleaseBinary
	if b = releaseInfo.getBinary(runtime.GOOS, runtime.GOARCH); len(b.URL) > 0 {
		return b, nil
	}
	return b, fmt.Errorf("doxa not available for %s/%s", runtime.GOOS, runtime.GOARCH)
}

// downloadFile downloads the compressed binary for the user's OS and architecture,
// extracts it, and copies it to the doxa/bin directory.
// It also moves the existing doxa and .ver files to doxa/bin/.previous,
// so they can be restored via the getdoxa restore command.
func downloadFile(URL string) error {
	extractPath := path.Join(tempDownload, "extracted")
	// just in case it exists, remove the previous extraction dir
	if DirExists(extractPath) {
		_ = os.RemoveAll(extractPath)
	}
	// Get release info from the remote repo
	response, err := http.Get(URL)
	if err != nil {
		return err
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(response.Body)

	if response.StatusCode != 200 {
		return errors.New("received non 200 response code")
	}
	// download the requested doxa tar gz file to the temp dir
	err = CreateDirs(tempDownload)
	if err != nil {
		return err
	}
	parts := strings.Split(URL, "/")
	urlFile := parts[len(parts)-1]
	filePath := path.Join(tempDownload, urlFile)
	//Create an empty file to copy contents into
	file, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(file)

	//Write the bytes to the file
	_, err = io.Copy(file, response.Body)
	if err != nil {
		return err
	}
	// extract the downloaded tar gz
	err = targz.Extract(filePath, extractPath)
	if err != nil {
		_ = os.RemoveAll(extractPath)
		log.Fatal(err)
	}
	// if exists, save old Doxa as bin/.previous/doxa
	if FileExists(currentDoxaFile) {
		// create .previous dir if it does not exist
		if !DirExists(previousDir) {
			err = CreateDirs(previousDir)
			if err != nil {
				log.Fatal(err)
			}
		}
		// move the doxa file to .previous
		err = os.Rename(currentDoxaFile, previousDoxaFile)
		if err != nil {
			_ = os.RemoveAll(extractPath)
			log.Fatal(err)
		}
		// move the .ver file to .previous
		if FileExists(localVersionFile) {
			err = os.Rename(localVersionFile, previousDoxaVersionFile)
			if err != nil {
				_ = os.RemoveAll(extractPath)
				log.Fatal(err)
			}
		}
	}
	// move downloaded doxa to doxa/bin
	downloadedDoxa := path.Join(extractPath, "doxa")
	err = os.Chmod(downloadedDoxa, 0700)
	if err != nil {
		_ = os.RemoveAll(extractPath)
		log.Fatal(err)
	}
	err = os.Rename(downloadedDoxa, currentDoxaFile)
	if err != nil {
		_ = os.RemoveAll(extractPath)
		log.Fatal(err)
	}
	// remove the tmp directory
	err = os.RemoveAll(tempDownload)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}

// FileExists returns true if the file indicated by the path exists
func FileExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// DirExists returns true if the final segment of the path exists and is a directory
func DirExists(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return info.IsDir()
}

// GetFileLines opens the file at the specified path and returns an array of its lines
func GetFileLines(path string) ([]string, error) {
	readFile, err := os.OpenFile(path, os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		return nil, err
	}
	fileScanner := bufio.NewScanner(readFile)
	fileScanner.Split(bufio.ScanLines)
	var fileTextLines []string

	for fileScanner.Scan() {
		fileTextLines = append(fileTextLines, fileScanner.Text())
	}

	err = readFile.Close()
	if err != nil {
		return nil, err
	}
	return fileTextLines, err
}

// getReleaseInfo reads the release.json file specified by the url,
// and populates releaseInfo from the json string.
func getReleaseInfo(url string) error {
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("GET error: %v", err)
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			log.Fatal(err)
		}
	}(resp.Body)

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("status error: %v", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("read body: %v", err)
	}

	err = json.Unmarshal(data, &releaseInfo)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

// ReleaseBinary holds the unmarshalled json information for an available binary
type ReleaseBinary struct {
	OS     string
	ARCH   string
	URL    string
	SHA256 string
}

// ReleaseInfo holds the unmarshalled json release information
type ReleaseInfo struct {
	Description string
	ReleaseId   string
	ReleaseInfo string
	BinaryMap   map[string]ReleaseBinary
}

// getBinary returns binary information for the specified OS and architecture
func (r *ReleaseInfo) getBinary(os, arch string) ReleaseBinary {
	key := fmt.Sprintf("%s/%s", os, arch)
	b := r.BinaryMap[key]
	return b
}

// CreateDirs creates the specified directory and parents in path if they do not exist.
// The error will be nil unless something is wrong with the path.
func CreateDirs(path string) error {
	var err error = nil
	if !DirExists(path) {
		err = os.MkdirAll(path, os.ModePerm)
	}
	return err
}

// GetInput uses the question (prompt) and options to obtain user input from the terminal
func GetInput(question string, options []string) rune {
	reader := bufio.NewReader(os.Stdin)
	sb := strings.Builder{}
	j := len(options)
	for i := 0; i < j; i++ {
		if i < j {
			sb.WriteString(options[i][0:1])
			i++
		}
	}
	validChars := sb.String()
	var char rune
l:
	for {
		fmt.Println(question)
		for i := 0; i < j; i++ {
			if i < j {
				fmt.Printf("%s) %s\n", options[i], options[i+1])
				i++
			}
		}
		char, _, _ = reader.ReadRune()
		if strings.Contains(validChars, string(char)) {
			break l
		} else {
			fmt.Println("Invalid selection")
		}
	}
	return char
}
// install prompts the user to determine if he/she wants to install doxa or its newer version.
// If so, it will install it.
func install(from, to string) {
	var q string
	if len(from) > 0 {
		q = fmt.Sprintf("Upgrade doxa from version %s to %s?", from, to)
	} else {
		q = fmt.Sprintf("Install doxa version %s?", to)
	}
	var o = []string{
		"y", "yes",
		"n", "no",
	}
	switch GetInput(q, o) {
	case 'y':
		var err error
		var b ReleaseBinary
		if b, err = getReleaseBinaryInfo(); err != nil {
			fmt.Println(err)
		} else {
			err = downloadFile(b.URL)
			if err != nil {
				fmt.Println(err)
			}
			addDoxaBinPath()
			fmt.Printf("doxa has been installed in %s\n", doxaBin)
		}
	default:
		os.Exit(0)
	}
}
// restore moves doxa/bin/.previous/.doxa to doxa/bin/doxa
// and doxa/bin/.previous/.ver to doxa/bin/.ver
// It then removes doxa/bin/.previous
func restore() {
	if !FileExists(previousDoxaFile) {
		fmt.Println("Sorry, the previous version of doxa is not on your computer.")
		os.Exit(0)
	}
	q := "Restore doxa to the previous version?"
	var o = []string{
		"y", "yes",
		"n", "no",
	}
	switch GetInput(q, o) {
	case 'y':
		err := os.Rename(previousDoxaFile, currentDoxaFile)
		if err != nil {
			log.Fatal(err)
		}
		if FileExists(previousDoxaVersionFile) {
			err := os.Rename(previousDoxaVersionFile, localVersionFile)
			if err != nil {
				log.Fatal(err)
			}
		}
		_ = os.RemoveAll(previousDir)
		fmt.Println("doxa has been restored to the previous version")
	default:
		os.Exit(0)
	}
}

// addDoxaBinPath updates the PATH variable on the user's computer to include doxa/bin
func addDoxaBinPath() {
	var shellResourceFile string

	switch runtime.GOOS {
	case "darwin":
		shellResourceFile = path.Join(userHome, ".zshrc")
		addPath(shellResourceFile, fmt.Sprintf("export PATH=$PATH:\"%s\"", doxaBin))
	default:
		log.Printf("Can't update path to %s for %s\n", doxaBin, runtime.GOOS)
	}
}
// addPath appends the resource file with doxa bin path information
func addPath(resourceFile, doxaBinPath string) {
	// get the resource file lines
	lines, err := GetFileLines(resourceFile)
	if err != nil {
		fmt.Printf("Could not read %s: %v\n", resourceFile, err)
	}
	// process the lines
	var newLines []string
	var hasDoxaBin bool

	for _, line := range lines {
		if strings.Contains(line, "doxa/bin") {
			hasDoxaBin = true
			break
		}
		newLines = append(newLines, line)
	}
	if !hasDoxaBin {
		f, err := os.OpenFile(resourceFile,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
		if err != nil {
			log.Println(err)
		}
		defer func(f *os.File) {
			err := f.Close()
			if err != nil {
				log.Println(err)
				return
			}
		}(f)
		fmt.Printf("Appending doxa/bin to PATH variable in %s\n", resourceFile)
		if _, err := f.WriteString(fmt.Sprintf("%s\n", doxaBinPath)); err != nil {
			log.Println(err)
			return
		}
		if runtime.GOOS == "darwin" {
			fmt.Println("Either exit the terminal and restart it, or type `source ~/.zshrc` then press the ENTER key.")
		}
	}
}
