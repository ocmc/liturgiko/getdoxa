# getDoxa

Installer / updater for [doxa](https://gitlab.com/ocmc/liturgiko/doxa)

To install doxa, you need to first download a tar gz file for your operating system and architecture.

Currently, getDoxa only supports Mac OS (aka darwin).  If you have an M1 Mac, use the darwin/arm64 binary.  Otherwise, use the darwin/amd64 binary.

The release binaries and instructions are available [here](https://github.com/liturgiko/getdoxa/releases)

The source code for getdoxa is [here](https://gitlab.com/ocmc/liturgiko/getdoxa).
